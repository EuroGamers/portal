jQuery( function ($) {
  'use strict';

  const socket = io.connect('http://192.168.43.228:3000');
  const app = {

    init: function () {
      this.list();
      this.actions();
      this.socketActions();
    },

    persist: function (new_article) {
      socket.emit('add', new_article);
    },

    edit: function (edit_article) {
      socket.emit('edit', edit_article);
    },

    destroy: function (article_id) {
      socket.emit('delete', { id: article_id });
    },

    changeStatus: function (article_id, article_status) {
      socket.emit('changestatus', { id: article_id, status: article_status });
    },

    actions: function () {
      $('#article-form').submit(function () {
        if (!$('#title').val()) {
          return false;
        }
        const new_article = {
          title: $('#title').val(),
          description: $('#description').val(),
          content: $('#content').val(),
          tags: $('#tags').val(),
          user_id: $('#user_id').val(),
          category_id: $('#category_id').val(),
          published: $('#published').val()
        }

        $('#title').val('');
        $('#description').val('');
        $('#content').val('');
        $('#tags').val('');
        $('category_id').val('');
        $('published').val('');

        app.persist(new_article);

        return false;
      });


    },

    socketActions: function () {
      socket.on('count', function (data) {
        if (data.count > 1) {
          $('#online-users').html(data.count+' usuarios en linea.');
        } else {
          $('#online-users').html(data.count+' usuario en linea.');
        }
      });

      socket.on('added', function (data) {
        app.addToList(data);
      });

      socket.on('deleted', function (data) {
        app.destroyOnArticleList(data.id);
      });

      socket.on('edited', function (data) {
        //TODO
      });

    },

    list: function () {
      socket.on('all', function (data) {
        if (document.getElementById('article-list')) {
          $('#article-list').html('');
          for (let i = 0; i < data.length; i++) {
            if (data[i].published) {
              if(data[i].tags) {
                $('#article-list').append('<div id="'+data[i]._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+data[i].image+'" alt=""></div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'"> '+data[i].title+'</a></h3></div><div class="tags"><i class="fa fa-tags"></i> '+data[i].tags+'</div><div class="description">'+data[i].description+'</div><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
              } else {
                $('#article-list').append('<div id="'+data[i]._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+data[i].image+'" alt=""></div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'"> '+data[i].title+'</a></h3></div><div class="description">'+data[i].description+'</div><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
              }
            }
          }
        }
        else if (document.getElementById('article-category-list')) {
          const id = $('#article-category-list').attr('data-catId');
          $('#article-category-list').html('');
          for (let i = 0; i < data.length; i++) {
              if (data[i].published && data[i].category_id === id) {
                if(data[i].tags) {
                  $('#article-category-list').append('<div id="'+data[i]._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+data[i].image+'" alt=""></div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'"> '+data[i].title+'</a></h3></div><div class="tags"><i class="fa fa-tags"></i> '+data[i].tags+'</div><div class="description">'+data[i].description+'</div><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
                } else {
                    $('#article-category-list').append('<div id="'+data[i]._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+data[i].image+'" alt=""></div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'"> '+data[i].title+'</a></h3></div><div class="description">'+data[i].description+'</div><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
                }
                console.log('categoria db: '+data[i].category_id+' vs categoria local '+ id);

              }
            }
          }
      });
      // socket.on('catlist', function (data) {
      //   $('#article-category-list').html('');
      //   for (let i = 0; i < data.length; i++) {
      //     if (data[i].published) {
      //       console.log('Articulo %i',i);
      //       $('#article-category-list').append('<div id="'+data[i]._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+data[i].image+'" alt=""></div><div class="over-info bottom h4">'+data[i].published_at+'</div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'"> '+data[i].title+'</a></h3></div><div class="tags"><i class="fa fa-tags"></i> '+data[i].tags+'</div><div class="description">'+data[i].description+'</div><a href="http://192.168.10.128:8000/support/article/'+data[i]._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
      //     }
      //   }
      // });
    },
    addToList: function (new_article) {
      $('#article-list').append('<div id="'+new_article._id+'" class="news-one row"><div class="col-md-4"><a href="#" class="angled-img"><div class="img"><img src="'+new_article.image+'" alt=""></div><div class="over-info bottom h4">'+new_article.published_at+'</div></a></div><div class="col-md-8"><div class="clearfix"><h3 class="h2 pull-left m-0"><a href="http://192.168.10.128:8000/support/article/'+new_article._id+'"> '+new_artice.title+'</a></h3></div><div class="tags"><i class="fa fa-tags"></i> '+new_artice.tags+'</div><div class="description">'+new_article.description+'</div><a href="http://192.168.10.128:8000/support/article/'+new_article._id+'" class="btn read-more pull-left">Leer mas</a></div></div>');
    }
  };
  window.App = app.init();
});
