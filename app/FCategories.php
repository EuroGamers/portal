<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class FCategories extends Model {

    protected $fillable = ['cat_name', 'position'];

    public function forums()
    {
        return $this->hasMany('eurogamers\Forums', 'cat_id');
    }

    public function topics()
    {
        return $this->hasManyThrough('eurogamers\Topics', 'eurogamers\Forums', 'cat_id', 'forum_id');
    }

}