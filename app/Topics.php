<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Topics extends Model {


    protected $fillable = ['poster', 'subject', 'first_post_id', 'last_post_id', 'last_poster', 'num_views', 'num_replies', 'closed', 'sticky', 'forum_id'];

    public function forum()
    {
        return $this->belongsTo('eurogamers\Forums', 'forum_id');
    }

    public function posts()
    {
        return $this->hasMany('eurogamers\Posts', 'topic_id');
    }

    public function autor()
    {
        return $this->belongsTo('eurogamers\User', 'poster');
    }

    public function lastposter()
    {
        return $this->belongsTo('eurogamers\User', 'last_poster');
    }

}
