<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Ranks extends Model {


    protected $fillable = ['rank', 'min_post', 'image'];

}
