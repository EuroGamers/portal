<?php namespace eurogamers\Events;

use eurogamers\Events\Event;
use eurogamers\Article;

use Illuminate\Queue\SerializesModels;

class ArticleWasViewed extends Event {

	use SerializesModels;
    public $article;
	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(Article $article)
	{
		$this->article = $article;
	}

}
