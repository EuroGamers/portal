<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class FPerms extends Model {


    protected $fillable = ['group_id', 'forum_id', 'read_forum', 'post_replies', 'post_topics'];

}
