<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Site\IndexController@index');

Route::get('support', 'Site\Support\SupportController@index');

Route::get('support/article/{article}', ['as' => 'article', 'uses' => 'Site\Support\SupportController@article']);

Route::get('support/cat/{category}', ['as' => 'category', 'uses' => 'Site\Support\SupportController@categoryindex']);

Route::get('forum', ['as' => 'forum', 'uses' => 'Site\Forum\ForumController@index']);

Route::get('forum/cat/{fcategory}', ['as' => 'fcategory', 'uses' => 'Site\Forum\ForumController@forumlist']);

Route::get('forum/topics/{forum}', ['as' => 'topics', 'uses' => 'Site\Forum\ForumController@topicslist']);

Route::get('forum/topic/{topic}/view', ['as' => 'topic.view', 'uses' => 'Site\Forum\ForumController@topicdetail']);

Route::get('usercp', 'Usercp\HomeController@index');

Route::get('admincp', 'Admincp\HomeController@index');

Route::get('metin2', 'Metin2\HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
