<?php namespace eurogamers\Http\Controllers\Usercp;

use eurogamers\Http\Controllers\Controller;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| Este controlador es el encargado de mostrar el dashboard para los usuarios autenticados.
	| Se usará temporalmente para construir el Usercp.
	*/

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('usercp.home');
	}

}
