<?php namespace eurogamers\Http\Controllers\Auth;
use Illuminate\Http\Request;
use eurogamers\User;
use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use eurogamers\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers, ThrottlesLogins {
        AuthenticatesAndRegistersUsers::postLogin as laravelPostLogin;
    }

	protected $redirectTo = '/usercp';
    protected $username = 'login';
	/**
	 * Create a new authentication controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest', ['except' => 'getLogout']);
	}

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'country' => 'required',
            'username' => 'required|max:16|min:4',
            'email' => 'required|email|max:255|unique:users',
            'birthday' => 'required',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    public function create(array $data)
    {
        return User::create([
            'real_name' => $data['first_name'],
            'login' => $data['username'],
            'geo' => $data['country'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Override postLogin default laravel method with my own.
     * @param Request $request
     * @return mixed
     */
    public function postLogin(Request $request) {
        $field = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'login';
        $request->merge([$field => $request->input('login')]);
        $this->username = $field;

        return self::laravelPostLogin($request);
    }


}
