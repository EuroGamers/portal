<?php namespace eurogamers\Http\Controllers\Site;

use eurogamers\Http\Controllers\Controller;

class IndexController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Index Controller
	|--------------------------------------------------------------------------
	|
	| Este controlador es el encargado de mostrar la pagina inicial a los usuarios invitados
	| por Ivan Díaz - Vie 31 de Julio - 2015.
	*/

	/*public function __construct()
	{
		$this->middleware('guest');
	}*/

	public function index()
	{
		return view('site.portal');
	}

}
