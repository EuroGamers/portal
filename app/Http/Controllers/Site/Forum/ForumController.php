<?php namespace eurogamers\Http\Controllers\Site\Forum;
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 20:35
 */

use eurogamers\Http\Controllers\Controller;
use eurogamers\FCategories;
use eurogamers\Forums;
use eurogamers\Topics;

class ForumController extends Controller {

    /**
     * description: TODO.
     */

    public function index()
    {
        $FCategories = FCategories::orderBy('position', 'ASC')->get();
        $Topics = Topics::orderBy('id', 'DESC')->get();
        return view('site.forum', compact('FCategories', 'Topics'));
    }

    public function forumlist(FCategories $category)
    {
        $FCategories = $category->orderBy('position', 'ASC')->get();
        $Topics = Topics::orderBy('id', 'DESC')->paginate(5);
        $Forums = $category->forums()->orderBy('position', 'ASC')->get();
        return view('site.forumcategory', compact('FCategories', 'Topics', 'Forums', 'category'));
    }

    public function topicslist(Forums $forum)
    {
        $FCategories = FCategories::orderBy('position', 'ASC')->get();
        $Sticky = $forum->topics()->where('sticky', 1)->paginate(5);
        $Topics = $forum->topics()->orderBy('id', 'DESC')->paginate(5);
        return view('site.forumtopics', compact('FCategories', 'Topics', 'forum', 'Sticky'));
    }

    public function topicdetail(Topics $topic)
    {
        $FCategories = FCategories::orderBy('position', 'ASC')->get();
        $Topics = Topics::orderBy('id', 'DESC')->paginate(5);
        $Posts = $topic->posts()->orderBy('id', 'ASC')->paginate(5);
        return view('site.forumtopic', compact('FCategories', 'Topics', 'Posts', 'topic'));
    }

}
