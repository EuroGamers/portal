<?php namespace eurogamers\Http\Controllers\Site\Support;
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 04/08/15
 * Time: 20:56
 * TODO
 */
use Event;
use eurogamers\Events\ArticleWasViewed;
use eurogamers\Http\Controllers\Controller;
use eurogamers\Category;
use eurogamers\Article;

class SupportController extends Controller {

    public function index()
    {
        $articles = Article::orderBy('published_at', 'desc')->paginate(6);
        return view('site.blog', compact('articles'));
    }

    // BEST METHODS IMPLEMENTATION, TODO
    public function categoryindex(Category $category)
    {
        $articles = $category->articles()->published()->orderBy('published_at', 'desc')->paginate(5);
        return view('site.blogcategory', compact('articles', 'category'));
    }

    public function article(Article $article)
    {
        Event::fire(new ArticleWasViewed($article));
        return view('site.blogarticle', compact('article'));
    }
}
