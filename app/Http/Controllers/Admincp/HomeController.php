<?php namespace eurogamers\Http\Controllers\Admincp;

use eurogamers\Http\Controllers\Controller;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| Este controlador es el encargado de mostrar el dashboard para los administradores autenticados.
	| Se usará temporalmente para construir el Admincp.
	*/

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('usercp.home');
	}

}
