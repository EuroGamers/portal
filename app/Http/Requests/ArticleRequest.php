<?php namespace eurogamers\Http\Requests;

use eurogamers\Http\Requests\Request;

class ArticleRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'title' => 'required|min:3',
			'description' => 'required|max:160',
			'category_id' => 'required',
			'content' => 'required',
			'tags' => 'required|min:6'
			'published_at' => 'required|date'
		];
	}

}
