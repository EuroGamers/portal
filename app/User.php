<?php namespace eurogamers;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $connection = 'account';
	protected $table = 'account';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['real_name','geo','birthday','email','password', 'last_visit', 'web_ip', 'status', 'coins', 'drs', 'picture', 'login', 'group_id', 'firma'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token', 'social_id'];

	public function notifications()
	{
		return $this->hasMany('eurogamers\Notification');
	}

	public function articles()
	{
		return $this->hasMany('eurogamers\Article');
	}

	public function newNotification()
	{
		$notification = new Notification;
		$notification->user()->associate($this);
		return $notification;
	}

	public function getLastVisitAttribute($date)
	{
		return Carbon::parse($date);
	}

	public function setIpAddressAttribute($ip)
	{
		$this->attributes['ip_address'] = inet_pton($ip);
	}

	public function getIpAddressAttribute($ip)
	{
		return inet_ntop($ip);
	}

	public function group()
    {
        return $this->hasOne('eurogamers\FGroups', 'g_id');
    }


}
