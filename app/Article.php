<?php namespace eurogamers;

//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model;
use Carbon\Carbon;

class Article extends Model {
	protected $connection = 'api';

	//protected $collection = 'articles';
	protected $primaryKey = '_id';
	protected $fillable = ['category_id', 'title','content', 'description', 'read', 'published_at', 'user_id', 'published'];


    public function category()
    {
        return $this->belongsTo('eurogamers\Category');
    }


    public function getPublishedAtAttribute($date)
    {
			$hour = Carbon::parse($date);
			if ($hour->diffInSeconds() < 59) {
				return "Hace ". Carbon::parse($date)->diffInSeconds() . " segundos";
			}
			else if ($hour->diffInSeconds() > 59 && $hour->diffInMinutes() < 59) {
				return "Hace ". Carbon::parse($date)->diffInMinutes() . " minutos";
			}
			else if ($hour->diffInMinutes() > 59 && $hour->diffInHours() < 24) {
				return "Hace ". Carbon::parse($date)->diffInHours() . " horas";
			}
			elseif ($hour->diffInHours() > 24 ) {
				return "Hace ". Carbon::parse($date)->diffInDays() . " días";
			}

    }

    public function user()
    {
        return $this->belongsTo('eurogamers\User');
    }

    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

}
