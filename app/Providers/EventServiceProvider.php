<?php namespace eurogamers\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'auth.login' => [
            'eurogamers\Listeners\SetUserLoginCredentials',
        ],
        'auth.logout' => [
            'eurogamers\Listeners\SetUserLogoutCredentials',
        ],
		'eurogamers\Events\ArticleWasViewed' => [
			'eurogamers\Listeners\IncrementArticleViews',
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);

		//
	}

}
