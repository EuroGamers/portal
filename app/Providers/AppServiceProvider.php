<?php namespace eurogamers\Providers;

use Illuminate\Support\Facades\Schema;
use View;
use eurogamers\Category;
use eurogamers\Settings;
use eurogamers\Article;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        if ((Schema::hasTable('categories')) && (Schema::hasTable('settings')) && (Schema::hasTable('articles')))
        {
		$categories = Category::orderBy('name', 'desc')->get();
		$popular = Article::orderBy('read', 'desc')->orderBy('published_at', 'desc')->get();
        $settings = Settings::all();
		View::share('catlist', $categories);
		View::share('popular', $popular);
        View::share('settings', $settings);
        }
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'eurogamers\Services\Registrar'
		);
	}

}
