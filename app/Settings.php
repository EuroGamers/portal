<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model {


    protected $fillable = ['logo', 'site_title', 'blog_title', 'forum_title', 'email', 'facebook', 'twitter', 'google_plus', 'youtube', 'analytics_id', 'disqus_shortname'];
}
