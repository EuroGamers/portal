<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class FGroups extends Model {


    protected $fillable = ['g_title', 'g_user_title', 'g_moderator', 'g_mod_edit_users', 'g_mod_rename_users', 'g_mod_change_passwords', 'g_mod_ban_users', 'g_read_board', 'g_view_users', 'g_post_replies', 'g_post_topics', 'g_edit_posts', 'g_delete_posts', 'g_delete_topics', 'g_set_title', 'g_search', 'g_search_users', 'g_send_email', 'g_post_flood', 'g_search_flood', 'g_email_flood'];

    public function users()
    {
        return $this->hasMany('eurogamers\Users', 'group_id');
    }
}
