<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Forums extends Model {

    protected $fillable =
        ['forum_name', 'forum_desc', 'num_topics', 'num_posts',
            'last_post', 'last_post_id',
            'last_poster', 'position', 'cat_id'];

    public function category(){
        return $this->belongsTo('eurogamers\FCategories', 'cat_id');
    }

    public function topics()
    {
        return $this->hasMany('eurogamers\Topics', 'forum_id');
    }

    public function posts()
    {
        return $this->hasManyThrough('eurogamers\Posts', 'eurogamers\Topics', 'forum_id', 'topic_id');
    }

    public function autor()
    {
        return $this->belongsTo('eurogamers\User', 'last_poster');
    }

}
