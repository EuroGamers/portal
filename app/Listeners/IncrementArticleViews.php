<?php

namespace eurogamers\Listeners;

use eurogamers\Events\ArticleWasViewed;

class IncrementArticleViews {

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleWasViewed  $event
     * @return void
     */
    public function handle(ArticleWasViewed $event)
    {
        $event->article->read++;
        $event->article->save();
    }

}
