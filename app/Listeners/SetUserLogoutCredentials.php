<?php
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 20/08/15
 * Time: 11:50
 */
namespace eurogamers\Listeners;
use eurogamers\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
class SetUserLogoutCredentials
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * Create the event handler.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Update authenticated user's logout timestamp
     *
     * @param User $user
     * @return void
     */
    public function handle(User $user)
    {
        $user->last_visit = Carbon::now();
        $user->save();
    }
}