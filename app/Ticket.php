<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {


	protected $fillable = ['user_first_name', 'category_id', 'title','description', 'user_email', 'user_telephone'];

	public function category()
    {
        return $this->belongsTo('eurogamers\Category');
    }
}
