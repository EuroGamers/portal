<?php namespace eurogamers;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model;

class Category extends Model {

	// protected $table = 'categories';
	protected $connection = 'api';

	protected $fillable = ['name'];

	public function articles()
	{
		return $this->hasMany('eurogamers\Article');
	}

	public function tickets()
	{
		return $this->hasMany('eurogamers\Ticket');
	}

}
