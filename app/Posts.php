<?php namespace eurogamers;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {


    protected $fillable = ['poster', 'poster_id', 'poster_ip', 'poster_email', 'message', 'edited_by', 'topic_id'];

    public function topic()
    {
        return $this->belongsTo('eurogamers\Topics');
    }

    public function autor()
    {
        return $this->belongsTo('eurogamers\User', 'poster_id');
    }

}
