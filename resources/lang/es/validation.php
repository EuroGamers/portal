<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validacion - Strings
	|--------------------------------------------------------------------------
	|
	| Las siguientes líneas lingüísticas contienen los mensajes de error por defecto utilizados por la clase validador.
	| Algunas de estas normas tienen varias versiones, como las normas de tamaño. 
	| No dudes en traducir cada uno de estos mensajes aquí.
	| por Carlos Escobar - Jue 30 de Julio - 2015.
	*/

	"accepted"             => "El :attribute debe ser aceptado.",
	"active_url"           => "El :attribute no es una URL valida.",
	"after"                => "El :attribute debe ser una fecha despues de :date.",
	"alpha"                => "El :attribute solo debe contener letras.",
	"alpha_dash"           => "El :attribute solo debe contener letras, numeros y guiones.",
	"alpha_num"            => "El :attribute solo debe contener letras y numeros.",
	"array"                => "El :attribute debe ser un arreglo.",
	"before"               => "El :attribute debe ser una fecha antes de :date.",
	"between"              => [
		"numeric" => "El :attribute debe estar entre :min y :max.",
		"file"    => "El :attribute debe estar entre :min y :max kilobytes.",
		"string"  => "El :attribute debe estar entre :min y :max caracteres.",
		"array"   => "El :attribute debe estar entre :min y :max items.",
	],
	"boolean"              => "El :attribute campo debe ser falso o verdadero.",
	"confirmed"            => "El :attribute confirmación no coincide.",
	"date"                 => "El :attribute no es una fecha valida.",
	"date_format"          => "El :attribute no coincide con el formato :format.",
	"different"            => "El :attribute y :other deben ser diferentes.",
	"digits"               => "El :attribute debe contener :digits digitos.",
	"digits_between"       => "El :attribute debe estar entre :min y :max digitos.",
	"email"                => "El :attribute debe ser una dirección de correo valida.",
	"filled"               => "El campo :attribute es requerido.",
	"exists"               => "El :attribute seleccionado no es valido.",
	"image"                => "El :attribute debe ser una imagen.",
	"in"                   => "El :attribute seleccionado no es valido.",
	"integer"              => "El :attribute debe ser un entero.",
	"ip"                   => "El :attribute debe ser una IP valida.",
	"max"                  => [
		"numeric" => "El :attribute no puede ser mayor que :max.",
		"file"    => "El :attribute no puede ser mayor de :max kilobytes.",
		"string"  => "El :attribute no puede ser mayor de :max caracteres.",
		"array"   => "El :attribute no puede tener mas de :max items.",
	],
	"mimes"                => "El :attribute debe ser un archivo de type: :values.",
	"min"                  => [
		"numeric" => "El :attribute debe ser de al menos :min.",
		"file"    => "El :attribute debe ser de al menos :min kilobytes.",
		"string"  => "El :attribute debe ser de al menos :min caracteres.",
		"array"   => "El :attribute debe tener al menos :min items.",
	],
	"not_in"               => "El :attribute seleccionado no es valido.",
	"numeric"              => "El :attribute debe ser un numero.",
	"regex"                => "El :attribute formato no es valido.",
	"required"             => "El campo :attribute es requerido.",
	"required_if"          => "El campo :attribute es requerido cuando :other es :value.",
	"required_with"        => "El campo :attribute es requerido cuando :values está presente.",
	"required_with_all"    => "El campo :attribute es requerido cuando :values están presentes.",
	"required_without"     => "El campo :attribute es requerido cuando :values no esta presente.",
	"required_without_all" => "El campo :attribute es requerido cuando :values no estan presentes.",
	"same"                 => "El :attribute y :other deben coincidir.",
	"size"                 => [
		"numeric" => "El :attribute debe ser :size.",
		"file"    => "El :attribute debe ser :size kilobytes.",
		"string"  => "El :attribute debe ser :size caracteres.",
		"array"   => "El :attribute debe contener :size items.",
	],
	"unique"               => "El :attribute ya existe.",
	"url"                  => "El :attribute formato es invalido.",
	"timezone"             => "El :attribute debe ser una zona valida.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
