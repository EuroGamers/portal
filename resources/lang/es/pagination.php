<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Paginación - Strings
	|--------------------------------------------------------------------------
	|
	| por Carlos Escobar - Jue 30 de Julio - 2015
	|
	*/

	'previous' => '&laquo; Anterior',
	'next'     => 'Siguiente &raquo;',

];
