<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Recuperación de contraseñas - Strings
	|--------------------------------------------------------------------------
	| por Carlos Escobar - Jue 30 de Julio - 2015
	|
	*/

	"password" => "Las contraseñas deben tener al menos seis caracteres y que coincida con la confirmación.",
	"user" => "No podemos encontrar un usuario con ese e-mail.",
	"token" => "Este token de restablecimiento de contraseña no es válida.",
	"sent" => "Hemos enviado un correo con su enlace de restablecimiento de contraseña!",
	"reset" => "Tu contraseña ha sido restablecida!",

];
