@extends('layouts.blank')
@section('title')
    Mantenimiento
@endsection
@section('content')
	<section class="content-wrap full youplay-404">

		<!-- Banner -->
		<div class="youplay-banner banner-top">
			<div class="image" style="background-image: url(/images/404.jpg)">
			</div>

			<div class="info">
				<div>
					<div class="container align-center">
						<h2 class="h1">Lo sentimos</h2>
						<h3>Estamos en mantenimiento ;(</h3>
					</div>
				</div>
			</div>
		</div>
		<!-- /Banner -->

	</section>
	<!-- /Main Content -->
@endsection