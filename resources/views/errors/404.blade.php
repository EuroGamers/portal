@extends('layouts.full')
@section('title')
    Página no encontrada
@endsection
@section('content')
    <section class="content-wrap full youplay-404">

        <!-- Banner -->
        <div class="youplay-banner banner-top">
            <div class="image" style="background-image: url(/images/404.jpg)">
            </div>

            <div class="info">
                <div>
                    <div class="container align-center">
                        <h2 class="h1">404</h2>
                        <h3>Pagina no encontrada ;(</h3>


                        <form action="buscar.html">
                            <div class="youplay-input">
                                <input type="text" name="search" placeholder="Buscar en el sitio...">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Banner -->

    </section>
    <!-- /Main Content -->
@endsection