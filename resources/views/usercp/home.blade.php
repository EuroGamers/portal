
@extends('layouts.full')
@section('title')
	Usercp |
	@foreach($settings as $row)
		{{$row->site_title}}
	@endforeach
@endsection
@section('content')
	<section class="content-wrap">
        <!-- Banner -->
        <div class="youplay-banner banner-top small">
            <div class="image" style="background-image: url(/images/game-diablo-iii-1400x656.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
            </div>

            <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
                <div>
                    <div class="container youplay-user">
                        <a href="{{Auth::user()->picture}}" class="angled-img image-popup">
                            <div class="img">
                                <img src="{{Auth::user()->picture}}" alt="">
                            </div>
                            <i class="fa fa-search-plus icon"></i>
                        </a>
                        <!--
                                -->
                        <div class="user-data">
                            <h2>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</h2>
                            <div class="location"><i class="fa fa-map-marker"></i> {{Auth::user()->country}}</div>
                            <div class="activity">
                                <div>
                                    <div class="num">{{count(Auth::user()->articles)}}</div>
                                    <div class="title">Posts</div>
                                </div>
                                <div>
                                    <div class="num">2</div>
                                    <div class="title">Juegos</div>
                                </div>
                                <div>
                                    <div class="num">0</div>
                                    <div class="title">Seguidores</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Banner -->


        <div class="container youplay-content">

            <div class="row">

                <!-- Left Side -->
                <div class="col-md-4 pr-40">
                    <div class="side-block">
                        <a href="#!" class="btn btn-default btn-full">Sigueme</a>
                    </div>
                    <div class="side-block">
                        <h4 class="block-title">Acerca de</h4>
                        <div class="block-content">
                        {{Auth::user()->signature}}
                        </div>
                    </div>
                    <div class="side-block">
                        <!--<h4 class="block-title">Location</h4>
                        <div class="block-content pt-5">
                            <div class="responsive-embed responsive-embed-16x9">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d423284.59051352815!2d-118.41173249999999!3d34.0204989!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c75ddc27da13%3A0xe22fdf6f254608f4!2z0JvQvtGBLdCQ0L3QtNC20LXQu9C10YEsINCa0LDQu9C40YTQvtGA0L3QuNGPLCDQodCo0JA!5e0!3m2!1sru!2sru!4v1430158354122"
                                        width="600" height="450"></iframe>
                            </div>
                        </div>-->
                    </div>
                </div>
                <!-- Left Side -->

                <div class="col-md-8">

                    <!-- Activity -->
                    <h2 class="mt-0">Actividad</h2>
                    <div class="youplay-timeline">

                    </div>
                    <!-- /Activity -->


                    <!-- Skills -->
                    <h2>Habilidades</h2>
                    @if(Auth::user()->first_name == 'Carlos')
                    JS:
                    <div class="progress youplay-progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div>

                    CSS:
                    <div class="progress youplay-progress">
                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            <span class="sr-only">20% Complete</span>
                        </div>
                    </div>

                    PHP:
                    <div class="progress youplay-progress">
                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            <span class="sr-only">60% Complete (warning)</span>
                        </div>
                    </div>

                    Photoshop:
                    <div class="progress youplay-progress">
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="sr-only">5% Complete (danger)</span>
                        </div>
                    </div>
                    @else
                            .
                    @endif
                    <!-- /Skills -->
                </div>
            </div>
        </div>
	</section>
@endsection