@extends('layouts.full')
@section('title')
    Ingresar
@endsection
@section('content')
	<section class="content-warp full youplay-login">
		<!-- Banner -->
		<div class="youplay-banner banner-top">
			<div class="image" style="background-image: url(/images/banner-bg.jpg)">
			</div>

			<div class="info">
				<div>
					<div class="container align-center">
						<div class="youplay-form">
							<h2>Ingreso</h2>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> Hemos encontrado algunos errores.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

							<div class="btn-group social-list dib">
								<a class="btn btn-default" title="Compartir en Facebook" href="#!"><i class="fa fa-facebook"></i></a>
								<a class="btn btn-default" href="#!" title="Compartir en Twitter"><i class="fa fa-twitter"></i></a>
								<a class="btn btn-default" href="#!" title="Compartir en Google Plus"><i class="fa fa-google-plus"></i></a>
							</div>

							<form method="POST" action="/auth/login">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
								<div class="youplay-input">
									<input type="text" name="username" placeholder="Email ó usuario">
								</div>
								<div class="youplay-input">
									<input type="password" name="password" placeholder="Contraseña">
								</div>
                                <div class="youplay-checkbox">
                                        <input type="checkbox" id="checkbox1" name="remember" checked>
									<label for="checkbox1">Recordarme?</label>
                                </div>
								<button type="submit" class="btn btn-default db">Ingresar</button>
                                <a href="/password/email">Olvidaste tu contraseña?</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /Banner -->

	</section>
@endsection
