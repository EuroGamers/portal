<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 13:15
 */
 -->

<!-- Right Side -->
<div class="col-md-3">

    <!-- Side Search -->
    <div class="side-block ">
        <p>Buscar noticias:</p>
        <form action="#">
            <div class="youplay-input">
                <input type="text" name="search" placeholder="Ingresa terminos relacionados">
            </div>
        </form>
    </div>
    <!-- /Side Search -->

    <!-- Side Categories -->
    <div class="side-block ">
        <h4 class="block-title">Categorias</h4>
        <ul class="block-content">
            @if(count($catlist))
                @foreach($catlist as $category)
                    <li><a href="{{route('category', $category->id)}}">{{$category->name}}</a></li>
                @endforeach
            @endif
        </ul>
    </div>
    <!-- /Side Categories -->

    <!-- Side Popular News -->
    <div class="side-block ">
        <h4 class="block-title">Temas de ayuda</h4>
        <div class="block-content p-0">
            @if(count($popular))
            @foreach($popular as $row)
                    <!-- Single News Block -->
            <div class="row youplay-side-news">
                <div class="col-xs-3 col-md-4">
                    <a href="#" class="angled-img">
                        <div class="img">
                            <img src="{{ is_null($row->image) ? $row->image : asset('/images/game-dark-souls-ii-500x375.jpg') }}" alt="">
                        </div>
                    </a>
                </div>
                <div class="col-xs-9 col-md-8">
                    <h4 class="ellipsis"><a href="{{ route('article', $row->id)}}" title="{{$row->title}}!">{{$row->title}}</a></h4>
                    <span class="date"><i class="fa fa-calendar"></i> {{$row->published_at}}</span>
                </div>
            </div>
            <!-- /Single News Block -->
            @endforeach
            @endif
        </div>
    </div>
    <!-- /Side Popular News -->
</div>
<!-- /Right Side -->
