<!--/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 14/08/15
 * Time: 13:06
 */ -->
@extends('layouts.full')
@section('title')
    {{$article->title}} |
    @foreach($settings as $row)
        {{$row->blog_title}}
    @endforeach
@endsection
@section('content')

<!-- Banner -->
<div class="youplay-banner banner-top xsmall">
    <div class="image" style="background-image: url(/images/banner-blog-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
    </div>

    <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
        <div>
            <div class="container">
                <h2>{{ $article->title }}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Banner -->


<div class="container youplay-news youplay-post">
    @include('site.blogside')
    <div class="col-md-9">
        <!-- Post Info -->
        <article class="news-one">

            <!-- Post Text -->
            <div class="description">
                <!-- Slider -->
                <div class="youplay-slider gallery-popup pull-right">
                    <a href="/images/game-dark-souls-ii-1920x1080.jpg" class="angled-img pull-left">
                        <div class="img">
                            <img src="/images/game-dark-souls-ii-500x375.jpg" alt="">
                        </div>
                        <i class="fa fa-search-plus icon"></i>
                    </a>
                    <a href="/images/game-dark-souls-ii-2-1920x1248.jpg" class="angled-img pull-left">
                        <div class="img">
                            <img src="/images/game-dark-souls-ii-2-500x375.jpg" alt="">
                        </div>
                        <i class="fa fa-search-plus icon"></i>
                    </a>
                    <a href="/images/game-dark-souls-ii-3-1920x1080.jpg" class="angled-img pull-left">
                        <div class="img">
                            <img src="/images/game-dark-souls-ii-3-500x375.jpg" alt="">
                        </div>
                        <i class="fa fa-search-plus icon"></i>
                    </a>
                    <a href="/images/game-dark-souls-ii-4-1280x720.jpg" class="angled-img pull-left">
                        <div class="img">
                            <img src="/images/game-dark-souls-ii-4-500x375.jpg" alt="">
                        </div>
                        <i class="fa fa-search-plus icon"></i>
                    </a>
                </div>
                <!-- /Slider -->

               <p>{{$article->description}}</p>
                <p>{{$article->content}}</p>
            </div>
            <!-- /Post Text -->

            <!-- Post Tags -->
            <div class="tags">
                <i class="fa fa-tags"></i> {{$article->tags}}
            </div>
            <!-- /Post Tags -->

            <!-- Post Meta -->
            <div class="meta">
                <div class="item">
                    <i class="fa fa-user meta-icon"></i>
                    Autor <a href="http://akgamers.com">{{$article->user->first_name}}</a>
                </div>
                <div class="item">
                    <i class="fa fa-calendar meta-icon"></i>
                    Publicado {{ $article->published_at }}
                </div>
                <div class="item">
                    <i class="fa fa-bookmark meta-icon"></i>
                    @if (!$article->category)
                      Sin categoria
                    @else
                    Categoria <a href="{{ route('category', $article->category->id) }}">{{$article->category->name}}</a>
                    @endif
                </div>
                <div class="item">
                    <i class="fa fa-eye meta-icon"></i>
                    Leido {{ $article->read }}
                </div>
            </div>
            <!-- /Post Meta -->

            <!-- Post Share -->
            <div class="btn-group social-list">
                <a class="btn btn-default" title="Compartir en Facebook" href="#!"><i class="fa fa-facebook"></i></a>
                <a class="btn btn-default" href="#!" title="Compartir en Twitter"><i class="fa fa-twitter"></i></a>
                <a class="btn btn-default" href="#!" title="Compartir en Google Plus"><i class="fa fa-google-plus"></i></a>
                <a class="btn btn-default" href="#!" title="Compartir en LinkedIn"><i class="fa fa-linkedin"></i></a>
                <a class="btn btn-default" href="#!" title="Compartir en Tumblr"><i class="fa fa-tumblr"></i></a>
            </div>
            <!-- /Post Share -->
        </article>
        <!-- /Post Info -->

        {{--<!-- Post Comments -->
        <div class="comments-block">
            <h3>Comentarios <small>(0)</small></h3>

            <!-- Comment Form -->
            <form action="#!" class="comment-form">
                <div class="comment-avatar pull-left">
                    <img src="/images/avatar.png" alt="">
                </div>
                <div class="comment-cont clearfix">
                    <div class="youplay-input">
                        <input type="text" name="username" placeholder="Tu nombre *">
                    </div>
                    <div class="youplay-input">
                        <input type="email" name="email" placeholder="Correo *">
                    </div>
                    <div class="youplay-textarea">
                        <textarea name="message" rows="5" placeholder="Comentario..."></textarea>
                    </div>
                    <button class="btn btn-default pull-right">Enviar</button>
                </div>
            </form>
            <!-- /Comment Form -->

            <ul class="comments-list">
                <li>Sin comentarios ;(</li>
            </ul>--}}

            <!-- Comments List
                  <ul class="comments-list">
                      <li>
                          <article>
                              <div class="comment-avatar pull-left">
                                  <img src="assets/images/avatar-user-1.png" alt="">
                              </div>
                              <div class="comment-cont clearfix">
                                  <a class="comment-author h4" href="#!">Kristen Bradley</a>
                                  <div class="date"><i class="fa fa-calendar"></i> 25 minutes ago</div>
                                  <div class="comment-text">
                                      Ludum mutavit. Verbum est ex. Et ... sunt occidat. Videtur quod est super omne oppidum. Quis transfretavit tu iratus es contudit cranium cum dolor apparatus. Qui curis! Modo nobis certamen est, qui non credunt at.
                                  </div>
                                  <a href="#!" class="pull-right">Reply</a>
                              </div>
                          </article>
                      </li>
                  </ul>
                  Comments /List -->
        </div>
        <!-- /Post Comments -->
    </div>

</div>
@endsection
