<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 19:29
 */
 -->
@extends('layouts.full')
@section('title')
{{$topic->subject}}
@endsection
@section('content')
<!-- Banner -->
<div class="youplay-banner banner-top xsmall">
    <div class="image" style="background-image: url(/images/banner-blog-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
    </div>

    <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
        <div>
            <div class="container">
                <h2>{{$topic->subject}}</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Banner -->


<div class="container youplay-content">

    <div class="col-md-9">

        <!-- Breadcrumb -->
        <div class="mt-10 mb-20 pull-left">
            <a href="{{url('/')}}">Inicio</a>
            <span class="fa fa-angle-right"></span>
            <a href="{{route('forum')}}">Foros</a>
            <span class="fa fa-angle-right"></span>
            <a href="{{route('fcategory', $topic->forum->category->id)}}">{{$topic->forum->category->cat_name}}</a>
            <span class="fa fa-angle-right"></span>
            <a href="{{route('topics', $topic->forum->id)}}">{{$topic->forum->forum_name}}</a>
            <span class="fa fa-angle-right"></span>
            <span>{{$topic->subject}}</span>
        </div>
        <!-- /Breadcrumb -->

        <div class="clearfix"></div>

        <p class="mb-30">Mostrando {{count($topic->posts)}} @if(count($topic->posts)>1) respuestas @else respuesta @endif - 1 de {{count($topic->posts)}} (de {{count($topic->posts)}} en total)</p>

        <!-- Forums List -->
        <ul class="youplay-forum mr-10">

            <li class="body">
                @foreach($Posts as $row)
                <div>
                    <div class="top">
                        <a class="author h5 pull-left" href="#!">{{$row->topic->subject}}</a>
                        <div class="date pull-right"><i class="fa fa-calendar"></i> {{$row->created_at}}</div>
                    </div>

                    <div class="pull-left">
                        <span>{{$row->autor->first_name}}</span><br>
                        <span>{{$row->autor->slogan}}</span>
                        <div class="avatar">
                        <img src="{{$row->autor->picture}}" alt="">
                        </div>
                        <span>{{$row->autor->group->g_user_title}}</span>
                    </div>
                    <div class="reply clearfix">
                        <div class="text">
                            <p>{!!$row->message!!}</p>
                        </div>
                        <a href="#!" class="pull-right">Responder</a>
                    </div>
                </div>
                @endforeach

            </li>

        </ul>
        <!-- /Forums List -->

        <div class="clearfix"></div>
        <p>Mostrando {{count($topic->posts)}} @if(count($topic->posts)>1) respuestas @else respuesta @endif - 1 de {{count($topic->posts)}} (de {{count($topic->posts)}} en total)</p>

    </div>

    <!-- Right Side -->
@include('site.forumside')
    <!-- /Right Side -->

</div>