<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 19:25
 */
 -->

@extends('layouts.full')
@section('title')
 {{$forum->forum_name}}
@endsection
@section('content')
<!-- Banner -->
    <div class="youplay-banner banner-top xsmall">
      <div class="image" style="background-image: url(/images/banner-blog-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
      </div>

      <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
        <div>
          <div class="container">
            <h2>{{$forum->forum_name}}</h2>
          </div>
        </div>
      </div>
    </div>
    <!-- /Banner -->


    <div class="container youplay-content">

      <div class="col-md-9">

        <!-- Breadcrumb -->
        <div class="mt-10 mb-20 pull-left">
          <a href="{{url('/')}}">Inicio</a>
          <span class="fa fa-angle-right"></span>
          <a href="{{route('forum')}}">Foros</a>
          <span class="fa fa-angle-right"></span>
          <a href="{{route('fcategory', $forum->category->id)}}">{{$forum->category->cat_name}}</a>
          <span class="fa fa-angle-right"></span>
            <span>{{$forum->forum_name}}</span>

        </div>
        <!-- /Breadcrumb -->

        <div class="clearfix"></div>

        <!-- Forums List -->
        <ul class="youplay-forum mr-10">
          <li class="header">
            <ul>
              <li class="cell-icon"></li>
              <li class="cell-info">Tema</li>
              <li class="cell-topic-count">Voices</li>
              <li class="cell-reply-count">Posts</li>
              <li class="cell-freshness">Reciente</li>
            </ul>
          </li>

          <li class="body">
              @foreach($Sticky as $row)
                  <ul class="sticky">
                      <li class="cell-icon">
                          <i class="fa fa-thumb-tack"></i>
                      </li>
                      <li class="cell-info">
                          <a href="{{route('topic.view', $row->id)}}" class="title h4">{{$row->subject}}</a>
                          <div class="description">
                              Iniciado por:
                              <a href="#!">
                                  <img alt="" src="{{$row->autor->picture}}" height="15" width="15">{{$row->autor->first_name}}
                              </a>
                          </div>
                      </li>
                      <li class="cell-topic-count">{{count($Topics)}}</li>
                      <li class="cell-reply-count">{{count($row->posts)}}</li>
                      <li class="cell-freshness">
                          <a href="#!">{{$row->last_post}}</a>
                          <p>
                              <a href="#!">
                                  <img alt="" src="{{$row->lastposter->picture}}" height="25" width="25">{{$row->lastposter->first_name}}
                              </a>
                          </p>
                      </li>
                  </ul>
              @endforeach
              @foreach($Topics as $row)
                @if($row->sticky==0 && $row->closed==0)
                    <ul>
              <li class="cell-icon">
                <i class="fa fa-folder-open-o"></i>
              </li>
              <li class="cell-info">
                <a href="{{route('topic.view', $row->id)}}" class="title h4">{{$row->subject}}</a>
                <div class="description">
Iniciado por:
                  <a href="#!">
                    <img alt="" src="{{$row->autor->picture}}" height="15" width="15">{{$row->autor->first_name}}
</a>
                </div>
              </li>
              <li class="cell-topic-count">{{count($Topics)}}</li>
              <li class="cell-reply-count">{{count($row->posts)}}</li>
              <li class="cell-freshness">
                <a href="#!">{{$row->last_post}}</a>
                <p>
                  <a href="#!">
                    <img alt="" src="{{$row->lastposter->picture}}" height="25" width="25">{{$row->lastposter->first_name}}
</a>
                </p>
              </li>
            </ul>
                @endif
                @if($row->closed==1 && $row->sticky==0)
                    <ul class="closed">
              <li class="cell-icon">
                <i class="fa fa-lock"></i>
              </li>
              <li class="cell-info">
                <a href="{{route('topic.view', $row->id)}}" class="title h4">{{$row->subject}}</a>
                <div class="description">
Iniciado por:
                  <a href="#!">
                    <img alt="" src="{{$row->autor->picture}}" height="15" width="15">{{$row->autor->first_name}}
</a>
                </div>
              </li>
              <li class="cell-topic-count">{{count($Topics)}}</li>
              <li class="cell-reply-count">{{count($row->posts)}}</li>
              <li class="cell-freshness">
                <a href="#!">{{$row->last_post}}</a>
                <p>
                  <a href="#!">
                    <img alt="" src="{{$row->lastposter->picture}}" height="25" width="25">{{$row->lastposter->first_name}}
</a>
                </p>
              </li>
            </ul>
                @endif
            @endforeach

          </li>

        </ul>
        <!-- /Forums List -->

        <div class="clearfix"></div>
      </div>

      @include('site.forumside')

    </div>
@endsection