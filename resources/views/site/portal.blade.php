@extends('layouts.full')
@section('title')
    Inicio |
    @foreach($settings as $row)
        {{$row->site_title}}
    @endforeach
@endsection
@section('content')
<section class="content-wrap">
    <!-- Banner -->
    <section class="youplay-banner banner-top">
        <div class="image" style="background-image: url(/images/banner-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
        </div>

        <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);">
            <div>
                <div class="container">
                    <img src="/images/logo-witcher-3-400x197.png" alt="">
                    <br><h2><span class="label">Juegos increibles!</span></h2>
                    <em>"Una de las mejores experiencias multijugador"</em>
                    <br>
                    <br>
                    <br>
                    <a class="btn btn-lg btn-primary" href="/auth/register">Unirse</a> <a style="color: #00b6ff;"href="/auth/login"><b>Ingresar</b></a>
                </div>
            </div>
        </div>
    </section>
    <!-- /Banner -->

    <!-- Images With Text -->
    <div class="youplay-carousel">
        <a class="angled-img" href="#">
            <div class="img">
                <img src="http://yt2.catv.net/newsphoto/yt2_np2565.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Nilon2</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="http://sp3.fotolog.com/photo/35/48/104/legend_fairy/1255378957984_f.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>World of Warcraft</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="http://imagenes.es.sftcdn.net/es/scrn/189000/189271/minecraft-20-700x393.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>Minecraft</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="https://mundogamespc.files.wordpress.com/2013/01/wpid-gtasa_1600.jpg" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>San Andreas</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
        <a class="angled-img" href="#!">
            <div class="img">
                <img src="http://media.bestofmicro.com/Z/P/396565/gallery/league-of-legends-front-mmomac_w_500.png" alt="">
            </div>
            <div class="over-info">
                <div>
                    <div>
                        <h4>League of Legends</h4>
                        <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <!-- /Images With Text -->

    <!-- Preorder
    <section class="youplay-banner">
        <div class="image" style="background-image: url(/images/banner-witcher-3.jpg);">
        </div>

        <div class="info container align-center">
            <div>
                <img src="/images/logo-witcher-3-400x197.png" alt="">

                <br>
                <br>


                <br>
                <br>
                <a class="btn btn-lg" href="#">Proximamente!</a>
            </div>
        </div>
    </section>
     /Preorder -->

</section>

<h2 class="container h1">Porque preferirnos?</h2>
<section class="youplay-features container">
    <div class="col-md-3 col-xs-12">
        <div class="feature angled-bg">
            <i class="fa fa-cc-visa"></i>
            <h3>Pagos</h3>
            <small>Más de 5 formas de pagos</small>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="feature angled-bg">
            <i class="fa fa-gamepad"></i>
            <h3>Juegos</h3>
            <small>Tendremos una larga lista de juegos</small>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="feature angled-bg">
            <i class="fa fa-money"></i>
            <h3>Precios</h3>
            <small>Los articulos más baratos de toda internet.</small>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        <div class="feature angled-bg">
            <i class="fa fa-users"></i>
            <h3>Comunidad</h3>
            <small>Una de las comunidades gamers más grandes</small>
        </div>
    </div>
</section>
@endsection