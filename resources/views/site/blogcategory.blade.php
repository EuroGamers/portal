<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 19:00
 */
 -->
@extends('layouts.full')
@section('title')
        {{$category->name}} |
@foreach($settings as $row)
{{$row->blog_title}}
@endforeach
@endsection
@section('content')
        <!-- Main Content -->
<section class="content-wrap">

    <!-- Banner -->
    <div class="youplay-banner banner-top small">
        <div class="image" style="background-image: url(/images/banner-blog-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
        </div>

        <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
            <div>
                <div class="container">
                    <h1>{{$category->name}}</h1>
                    <h2>@foreach($settings as $row){{$row->blog_title}}@endforeach</h2>
                </div>
            </div>
        </div>
    </div>
    <!-- /Banner -->


    <div class="container youplay-news">
        @include('site.blogside')

                <!-- News List -->
        <div class="col-md-9" id="article-category-list" data-catId="{{ $category->id }}" >
            {{-- @if(count($articles))
            @foreach($articles as $article)
                    <!-- Single News Block -->
            <div class="news-one row">
                <div class="col-md-4">
                    <a href="#" class="angled-img">
                        <div class="img">
                            <img src="{{ is_null($article->image) ? $article->image : asset('/images/game-dark-souls-ii-500x375.jpg') }}" alt="">
                        </div>
                        <div class="over-info bottom h4">{{$article->published_at}}</div>
                    </a>
                </div>
                <div class="col-md-8">
                    <div class="clearfix">
                        <h3 class="h2 pull-left m-0"><a href="{{ route('article', ['id' => $article->id])  }}">{{$article->title}}</a></h3>
                    </div>
                    <div class="tags">
                        <i class="fa fa-tags"></i> {{$article->tags}}
                    </div>
                    <div class="description">
                        {{$article->description}}
                    </div>
                    <a href="{{ route('article', ['id' => $article->id])  }}" class="btn read-more pull-left">Leer mas</a>
                </div>
            </div>
            <!-- /Single News Block -->
            @endforeach
            @endif --}}
                    <!-- Pagination -->
            <ul class="pagination">
                <li class="active"><a href="#!">1</a>
                </li>
                <li><a href="#!">2</a>
                </li>
                <li><a href="#!">3</a>
                </li>
                <li><a href="#!">4</a>
                </li>
                <li><a href="#!">Siguiente</a>
                </li>
            </ul>
            <!-- /Pagination -->
        </div>
        <!-- /News List -->

    </div>
@endsection
@section('scripts')
{{-- <script type="text/javascript">
jQuery(function ($) {
  'use strict';
    let id = $('#article-category-list').attr('data-catId');
    const socket = io.connect('http://192.168.10.128:3000');
    socket.emit('catlist', id);
});
</script> --}}
@endsection
