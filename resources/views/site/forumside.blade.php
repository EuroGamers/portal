<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 17/08/15
 * Time: 18:29
 */
 -->

<!-- Right Side -->
<div class="col-md-3">

    <!-- Side Search -->
    <div class="side-block right-side">
        <p>Buscar por juegos:</p>
        <form action="search.html">
            <div class="youplay-input">
                <input type="text" name="search" placeholder="ingresar terminos de busqueda">
            </div>
        </form>
    </div>
    <!-- /Side Search -->

    <!-- Side Categories -->
    <div class="side-block right-side">
        <h4 class="block-title">Categorias</h4>
        <ul class="block-content">
            @foreach($FCategories as $row)
                <li><a href="{{route('fcategory', $row->id)}}">{{$row->cat_name}}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- /Side Categories -->

    <!-- Side Recent News -->
    <div class="side-block right-side">
        <h4 class="block-title">Recientes</h4>
        <div class="block-content p-0">
            <!-- Single News Block -->
            @if(count($Topics))
                @foreach($Topics as $row)
                    <div class="row youplay-side-news">
                        <div class="col-xs-3 col-md-4">
                            <a href="#" class="angled-img">
                                <div class="img">
                                    <img src="{{$row->autor->picture}}" alt="">
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-9 col-md-8">
                            <h4 class="ellipsis"><a href="{{route('topic.view', $row->id)}}" title="{{$row->subject}}">{{$row->subject}}</a></h4>
                            <span class="price">por {{$row->autor->first_name}}</span>
                        </div>
                    </div>
                @endforeach
            @else
                Sin temas recientes
                @endif
                        <!-- /Single News Block -->
        </div>
    </div>
    <!-- /Side Popular News -->
</div>
<!-- /Right Side -->