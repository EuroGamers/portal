<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 15/08/15
 * Time: 19:23
 */
 -->
@extends('layouts.full')
@section('title')
@foreach($settings as $row)
{{$row->forum_title}}
@endforeach
@endsection
@section('content')
<!-- Banner -->
<div class="youplay-banner banner-top xsmall">
    <div class="image" style="background-image: url(/images/banner-blog-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
    </div>

    <div class="info" data-top="opacity: 1; transform: translate3d(0px,0px,0px);" data-top-bottom="opacity: 0; transform: translate3d(0px,150px,0px);" data-anchor-target=".youplay-banner.banner-top">
        <div>
            <div class="container">
                <h2>@foreach($settings as $setting){{$setting->forum_title}}@endforeach</h2>
            </div>
        </div>
    </div>
</div>
<!-- /Banner -->


<div class="container youplay-content">

    <div class="col-md-9">

        <!-- Breadcrumb -->
        <div class="mt-10 pull-left">
            <a href="{{url('/')}}">Inicio</a>
            <span class="fa fa-angle-right"></span>
            <span>Foros</span>
        </div>
        <!-- /Breadcrumb -->

        <!-- Search -->
        <form action="search.html" class="pull-right">
            <p>Buscar por foros:</p>
            <div class="youplay-input pull-left">
                <input type="text" name="search">
            </div>
            <button class="btn pull-right">Buscar</button>
        </form>
        <!-- /Search -->

        <div class="clearfix"></div>

        <!-- Forums List -->
        @foreach($FCategories as $row)
        <ul class="youplay-forum mr-10">
            <li class="header">
                <ul>
                    <li class="cell-icon"></li>
                    <li class="cell-info fa fa-folder-open-o"><a href="{{route('fcategory', $row->id)}}">{{$row->cat_name}}</a></li>
                    <li class="cell-topic-count">Temas</li>
                    <li class="cell-reply-count">Posts</li>
                    <li class="cell-freshness">Reciente</li>
                </ul>
            </li>

            @foreach($row->forums as $foros)
            <li class="body">
                <ul>
                    <li class="cell-icon">
                        <i class="fa fa-folder-open-o"></i>
                    </li>
                    <li class="cell-info">
                        <a href="{{route('topics', $foros->id)}}" class="title h4">{{$foros->forum_name}}</a>
                        <div class="description">{{$foros->forum_desc}}</div>
                        {{--
                            LISTA DE SUBCATEGORIAS INNECESARIAS.
                        <ul class="forums-list">
                            <li><a href="#!"><i class="fa fa-folder-open-o"></i></a>,</li>
                            </li>
                        </ul>--}}
                    </li>
                    <li class="cell-topic-count">{{ count($foros->topics) }}</li>
                    <li class="cell-reply-count">{{ count($foros->posts) }}</li>
                    <li class="cell-freshness">
                    @if(count($foros->topics))
                        <a href="#!">{{$foros->last_post}}</a>
                        <p>
                            <a href="#!">
                                <img alt="" src="{{$foros->autor->picture}}" height="25" width="25">{{$foros->autor->first_name}}
                            </a>
                        </p>
                        @else
                            Sin temas nuevos
                        @endif
                    </li>
                </ul>
            </li>
        @endforeach

        </ul>
        @endforeach
        <!-- /Forums List -->

        <div class="clearfix"></div>
    </div>
@include('site.forumside')

@endsection