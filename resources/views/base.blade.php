<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>

	<link rel="icon" type="image/png" href="/images/icon.png">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<!--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"/>-->
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" href="/css/portal.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/borja-loader.css" />
	<link href="/fonts/css.css" rel='stylesheet' type='text/css'>
	@yield('head')
	<!--[if lt IE 9]>
	<script src="/js/html5shiv.min.js"></script>
	<![endif]-->

</head>
<body>
	@yield('nav')
    @yield('body')
	<footer>
		@yield('foot')
	</footer>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/shapes-polyfill.min.js"></script>
  <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/js/skrollr.min.js"></script>
  <script type="text/javascript" src="/js/smoothscroll.js"></script>
  <script type="text/javascript" src="/js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="/js/jquery.countdown.min.js"></script>
  <script type="text/javascript" src="/js/youplay.min.js"></script>
	<script src="http://192.168.43.228:3000/socket.io/socket.io.js') } }"></script>
	<script type="text/javascript" src="/js/blog.js"></script>

  <script>
	  if(typeof youplay !== 'undefined') {
		  youplay.init({
			  smoothscroll: false,
		  });
	  }
  </script>

  <script type="text/javascript">
	  $(".countdown").each(function() {
		  $(this).countdown($(this).attr('data-end'), function(event) {
			  $(this).text(
					  event.strftime('%D days %H:%M:%S')
			  );
		  });
	  })
  </script>

	@yield('scripts')

</body>
</html>
