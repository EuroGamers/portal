@extends('base')
@section('head')
    @yield('includes')
@endsection
@section('nav')
    @include('layouts.navbar')
@endsection
@section('body')
    @yield('content')
@endsection
@section('foot')
    <div class="wrapper" style="background-image: url(/images/footer-bg.jpg)" data-bottom="transform:translate3d(0px,0px,0px);" data-bottom-top="transform:translate3d(0px,-200px,0px);" data-anchor-target="footer">
        @yield('footer')
        <div class="social">
            <div class="container" data-bottom-top="opacity: 0;" data-bottom="opacity: 1;">
                <h3>Conectate con <strong>@foreach($settings as $row){{$row->site_title}}@endforeach</strong></h3>

                <div class="row icons">
                    <div class="col-xs-6 col-sm-3">
                        <a href="http://@foreach($settings as $row){{$row->facebook}}@endforeach">
                            <i class="fa fa-facebook-square"></i>
                            <span>Danos me gusta!</span>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <a href="http://@foreach($settings as $row){{$row->twitter}}@endforeach">
                            <i class="fa fa-twitter-square"></i>
                            <span>Siguenos!</span>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <a href="http://@foreach($settings as $row){{$row->google_plus}}@endforeach">
                            <i class="fa fa-google-plus-square"></i>
                            <span>Danos +1!</span>
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-3">
                        <a href="http://@foreach($settings as $row){{$row->youtube}}@endforeach">
                            <i class="fa fa-youtube-square"></i>
                            <span>Miranos!</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <strong>@foreach($settings as $row){{$row->site_title}}@endforeach</strong> &copy; 2015. All rights reserved
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@yield('scripts')
@endsection
