@extends('base')
@section('body')
    @yield('content')
@endsection
@section('footer')
    <div class="wrapper" style="background-image: url(/images/footer-bg.jpg)" data-bottom="transform:translate3d(0px,0px,0px);" data-bottom-top="transform:translate3d(0px,-200px,0px);" data-anchor-target="footer">
        @yield('footer')
        <div class="copyright">
            <div class="container">
                <strong>@foreach($settings as $row){{$row->site_title}}@endforeach</strong> &copy; 2015. All rights reserved
            </div>
        </div>
    </div>
@endsection
