<!--
* Created by PhpStorm.
* User: Carlos
* Date: 03/08/2015
* Time: 10:54 AM
-->
        <!-- Preloader -->
<div class="page-preloader preloader-wrapp">
    <img src="@foreach($settings as $row){{$row->logo}}@endforeach" alt="">
    <div class="loader">Loading...</div>
</div>
<!-- /Preloader -->

<!-- Navbar -->
<nav class="navbar-youplay navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="off-canvas" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="@foreach($settings as $row){{$row->logo}}@endforeach" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                @if(count($catlist))
                <li class="dropdown dropdown-hover ">
                    <a href="{{action('Site\Support\SupportController@index')}}" class="dropdown-toggle" role="button" aria-expanded="false">
                        Juegos <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" style="width: 320px;">
                        <ul role="menu">
                                @foreach($catlist as $row)
                                    <li><a href="{{ route('category', ['id' => $row->id]) }}">{{$row->name}}</a></li>
                                @endforeach
                        </ul>
                    </div>
                </li>
                @else
                    <li><a href="{{ action('Site\Support\SupportController@index') }}">Soporte<span class="label">asistencia</span></a></li>
                @endif
                <li><a href="{{ action('Site\Forum\ForumController@index') }}">Foros <span class="label">opinión</span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <span class="fa fa-user" id="online-users"></span>
                <li><a href="{{ action('Site\Support\SupportController@index') }}">Soporte</a></li>
                @if (Auth::guest())
                    <li><a href="/auth/login">Ingresar</a></li>
                    <li><a href="/auth/register">Registrarse</a></li>
                @else
                    <li class="dropdown dropdown-hover ">
                        <a href="#!" class="dropdown-toggle" role="button" aria-expanded="false">
                            {{Auth::user()->first_name}} <span class="badge bg-default">2</span> <span class="caret"></span> <span class="label">{{Auth::user()->slogan}}</span>
                        </a>
                        <div class="dropdown-menu">
                            <ul role="menu">
                                <li><a href="/usercp">Usercp <span class="badge pull-right bg-warning">13</span></a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="#">Compras <span class="badge pull-right bg-default">3</span></a>
                                </li>
                                <li><a href="#">Mis tickets <span class="badge pull-right bg-default">3</span></a>
                                </li>
                                <li class="divider"></li>

                                <li><a href="/auth/logout">Salir</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<!-- /Navbar -->
