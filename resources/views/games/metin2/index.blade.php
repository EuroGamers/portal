<!--
/**
 * Created by PhpStorm.
 * User: carlos
 * Date: 27/08/15
 * Time: 12:47
 */
 -->
@extends('layouts.full')
@section('title')
    Metin2 |
    @foreach($settings as $row)
        {{$row->site_title}}
    @endforeach
@endsection
@section('content')
    <section class="content-wrap">
        <!-- Banner -->
        <section class="youplay-banner banner-top">
            <div class="image" style="background-image: url(/images/banner-bg.jpg)" data-top="background-position: 50% 0px;" data-top-bottom="background-position: 50% -200px;">
            </div>

            <section class="youplay-banner small">
                <div class="image skrollable skrollable-between" style="background-image: url(/images/banner-witcher-3.jpg); background-size: cover; background-position: 50% 4.46096654275092px;" data-top-bottom="background-position: 50% -150px;" data-bottom-top="background-position: 50% 150px;">
                </div>

                <div class="info container align-center">
                    <div>
                        <h2>Nilon2:<br> Proximamente.</h2>

                        <!-- See countdown init in bottom of the page -->
                        <div class="countdown h2 skrollable skrollable-before" data-end="2015/09/15"></div>

                        <br>
                        <br>
                        <a class="btn btn-lg" href="#!">Registrarse</a>
                    </div>
                </div>
            </section>


        </section>
        <!-- /Banner -->


    </section>

    <h2 class="container h1">Porque preferirnos?</h2>
    <section class="youplay-features container">
        <div class="col-md-3 col-xs-12">
            <div class="feature angled-bg">
                <i class="fa fa-cc-visa"></i>
                <h3>Pagos</h3>
                <small>Más de 5 formas de pagos</small>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="feature angled-bg">
                <i class="fa fa-gamepad"></i>
                <h3>Juegos</h3>
                <small>Tendremos una larga lista de juegos</small>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="feature angled-bg">
                <i class="fa fa-money"></i>
                <h3>Precios</h3>
                <small>Los articulos más baratos de toda internet.</small>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="feature angled-bg">
                <i class="fa fa-users"></i>
                <h3>Comunidad</h3>
                <small>Una de las comunidades gamers más grandes</small>
            </div>
        </div>
    </section>
@endsection