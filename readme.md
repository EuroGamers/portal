## AKGamers


AKGamers es una comunidad de juegos multijugador online, la cual busca establecerse como una empresa de servicios, este proyecto busca realizar un portal web donde se contará con todas las plataformas de los juegos de forma integrada.


## Características

El portal tendrá las siguientes características:

(x) Tienda integrada para todos los juegos

(x) Sistema de soporte mediante tickets

(x) Panel de administración integrado para todos los juegos y el portal.

(x) Sistema de usuarios (un usuario para todos los juegos y servicios)

(x) Entre otras que se iran analizando y añadiendo con el tiempo

## DevHistory

### Primer release:

- Foro
- Soporte
- Portal
- Autenticación y registro

### Segundo release:

- Tickets
- Usercp
- Nilon2 site

### Tercer Release
- AKStore
- 10% Admincp

## Desarrolladores.

Carlos Andrés Escobar


### Licencia

La aplicación AKGamers es de código cerrado, por lo cual no se permite ninguna de las libertades de uso y distribución a terceros.