<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id')->unique();
			$table->string('logo');
            $table->string('site_title');
            $table->string('blog_title');
            $table->string('forum_title');
			$table->string('email');
			$table->string('facebook');
			$table->string('twitter');
            $table->string('google_plus');
            $table->string('youtube');
			$table->string('disqus_shortname');
			$table->string('analytics_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
