<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFPermsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('f_perms', function(Blueprint $table)
		{
            $table->integer('group_id')->unsigned();
		    $table->foreign('group_id')->references('g_id')->on('f_groups')->onDelete('cascade');
            $table->integer('forum_id')->unsigned();
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
            $table->tinyInteger('read_forum')->default(1);
            $table->tinyInteger('post_replies')->default(1);
            $table->tinyInteger('post_topics')->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('f_perms');
	}

}
