<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forums', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('forum_name');
            $table->string('forum_desc');
            $table->integer('num_topics');
            $table->integer('num_posts');
            $table->date('last_post');
            $table->integer('last_post_id');
            $table->string('last_poster');
            $table->integer('position');
            $table->integer('cat_id')->unsigned();
            $table->foreign('cat_id')->references('id')->on('f_categories')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forums');
	}

}
