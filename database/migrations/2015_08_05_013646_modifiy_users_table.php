<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiyUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('last_visit');
            $table->binary('ip_address');
            $table->string('picture')->default('/images/profilepics/default.png');
            $table->string('slogan')->default('AK-Gamer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('last_visit');
            $table->dropColumn('ip_address');
            $table->dropColumn('picture');
            $table->dropColumn('slogan');
        });
    }
}