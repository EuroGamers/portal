<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('poster');
            $table->integer('poster_id')->unsigned();
            $table->foreign('poster_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('poster_ip');
            $table->string('poster_email');
            $table->mediumText('message');
            $table->string('edited_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
