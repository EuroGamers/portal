<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('topics', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('poster')->unsigned();
			$table->foreign('poster')->references('id')->on('users')->onDelete('cascade');
            $table->string('subject');
            $table->integer('first_post_id')->unsigned();
            $table->foreign('first_post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->integer('last_post_id')->unsigned();
            $table->foreign('last_post_id')->references('id')->on('posts')->onDelete('cascade');
            $table->integer('last_poster')->unsigned();
            $table->foreign('last_poster')->references('id')->on('users')->onDelete('cascade');
            $table->mediumInteger('num_views');
            $table->mediumInteger('num_replies');
            $table->tinyInteger('closed')->default(0);
            $table->tinyInteger('sticky')->default(0);
            $table->integer('forum_id')->unsigned();
            $table->foreign('forum_id')->references('id')->on('forums')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('topics');
	}

}
