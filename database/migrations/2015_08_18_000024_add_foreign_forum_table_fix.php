<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignForumTableFix extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('forums', function(Blueprint $table){
            $table->integer('last_poster')->unsigned();
            $table->foreign('last_poster')->references('id')->on('users')->ondelete('cascade');
        });

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('forums', function(Blueprint $table){
            $table->dropForeign('forums_last_poster_foreign');
            $table->dropColumn('last_poster');
        });
	}

}
