<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('f_groups', function(Blueprint $table)
		{
			$table->increments('g_id');
			$table->string('g_title');
            $table->string('g_user_title');
            $table->tinyInteger('g_moderator');
            $table->tinyInteger('g_mod_edit_users');
            $table->tinyInteger('g_mod_rename_users');
            $table->tinyInteger('g_mod_change_passwords');
            $table->tinyInteger('g_mod_ban_users');
            $table->tinyInteger('g_read_board');
            $table->tinyInteger('g_view_users');
            $table->tinyInteger('g_post_replies');
            $table->tinyInteger('g_post_topics');
            $table->tinyInteger('g_edit_posts');
            $table->tinyInteger('g_delete_posts');
            $table->tinyInteger('g_delete_topics');
            $table->tinyInteger('g_set_title');
            $table->tinyInteger('g_search');
            $table->tinyInteger('g_search_users');
            $table->tinyInteger('g_send_email');
            $table->tinyInteger('g_post_flood');
            $table->tinyInteger('g_search_flood');
            $table->tinyInteger('g_email_flood');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('f_groups');
	}

}
