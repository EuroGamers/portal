<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserForumField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
        {
            $table->string('username')->unique();
            $table->integer('group_id')->unsigned();
            $table->foreign('group_id')->references('g_id')->on('f_groups')->onDelete('cascade');
            $table->string('signature');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table){
            $table->dropColumn('username');
            $table->dropForeign('users_group_id_foreign');
            $table->dropColumn('group_id');
            $table->dropColumn('signature');
        });
	}

}
