<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyArticlesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('articles', function(Blueprint $table)
		{
            $table->text('content');
            $table->integer('read')->default(0);
            $table->date('published_at');
            $table->string('tags');
            $table->string('image');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('articles', function(Blueprint $table)
	    {
		    $table->dropColumn('content');
            $table->dropColumn('read');
            $table->dropColumn('published_at');
            $table->dropColumn('tags');
            $table->dropColumn('image');
	    });
	}
}
