<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use eurogamers\Settings;
use eurogamers\Category;
use eurogamers\FGroups;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call('SettingsSeeder');
        $this->call('CategoriesSeeder');
        $this->call('GroupsSeeder');
		$this->command->info('Configuraciones por defecto establecidas!');
        $this->command->info('Categorias por defecto creadas');
        $this->command->info('Grupos por defecto creados');
	}
}

class SettingsSeeder extends Seeder {
    public function run(){
        Settings::create([
            'logo' => '/images/logo2.png',
            'site_title' => 'AKGamers',
            'blog_title' => 'Soporte',
            'forum_title' => 'Foros',
            'email' => 'info@akgamers.com',
            'facebook' => 'www.facebook.com/akgamers',
            'twitter' => 'www.twitter.com/akgamers',
            'google_plus' => 'www.google.com/akgamers',
            'youtube' => 'www.youtube.com/akgamers',
            'disqus_shortname' => '',
            'analytics_id' => ''
        ]);
    }
}

class CategoriesSeeder extends Seeder {
    public function run(){
        Category::create([
            'name' => 'Nilon2'
        ]);
        Category::create([
            'name' => 'World of Warcraft'
        ]);
        Category::create([
            'name' => 'AKGamers'
        ]);
    }
}

class GroupsSeeder extends Seeder{
    public function run(){
        FGroups::create([
            'g_title' => 'Usuarios',
            'g_user_title' => 'Usuario',
            'g_moderator' => 0,
            'g_mod_edit_users' => 0,
            'g_mod_rename_users' => 0,
            'g_mod_change_passwords' => 0,
            'g_mod_ban_users' => 0,
            'g_read_board' => 1,
            'g_view_users' => 1,
            'g_post_replies' => 1,
            'g_post_topics' => 1,
            'g_edit_posts' => 1,
            'g_delete_posts' => 1,
            'g_delete_topics' => 1,
            'g_search' => 1,
            'g_search_users' => 1,
            'g_send_email' => 1,
            'g_post_flood' => 30,
            'g_search_flood' => 30,
            'g_email_flood' => 60
        ]);
    }
}
